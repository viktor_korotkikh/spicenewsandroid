package com.viktorkorotkikh.spicenews.ui;


import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.octo.android.robospice.spicelist.SpiceListItemView;
import com.viktorkorotkikh.spicenews.R;
import com.viktorkorotkikh.spicenews.model.News;

public class NewsItemView extends RelativeLayout implements SpiceListItemView<News> {

    private static final int IMAGE_VIEW_COUNT = 1;

    private News newsItem;

    private ImageView image;
    private TextView title;

    public NewsItemView(Context context) {
        super(context);
        inflateView(context);
    }

    private void inflateView(Context context) {
        LayoutInflater.from(context).inflate(R.layout.view_newsitem, this);
        this.title = (TextView) this.findViewById(R.id.newsitem_title_textview);
        this.image = (ImageView) this.findViewById(R.id.newsitem_imageview);
    }


    @Override
    public News getData() {
        return newsItem;
    }

    @Override
    public ImageView getImageView(int imageIndex) {
        return image;
    }

    @Override
    public int getImageViewCount() {
        return IMAGE_VIEW_COUNT;
    }

    @Override
    public void update(News data) {
        this.newsItem = data;
        title.setText(data.getTitle());
    }
}
