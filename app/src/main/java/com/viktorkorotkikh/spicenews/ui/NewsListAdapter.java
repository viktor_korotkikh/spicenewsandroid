package com.viktorkorotkikh.spicenews.ui;


import android.content.Context;
import android.view.ViewGroup;

import com.octo.android.robospice.request.simple.BitmapRequest;
import com.octo.android.robospice.spicelist.SpiceListItemView;
import com.octo.android.robospice.spicelist.simple.BitmapSpiceManager;
import com.octo.android.robospice.spicelist.simple.SpiceArrayAdapter;
import com.viktorkorotkikh.spicenews.model.News;

import java.io.File;
import java.util.List;

public class NewsListAdapter extends SpiceArrayAdapter<News> {

    protected NewsListAdapter(Context context, BitmapSpiceManager spiceManagerBitmap, List<News> newsList) {
        super(context, spiceManagerBitmap, newsList);
    }

    @Override
    public SpiceListItemView createView(Context context, ViewGroup parent) {
        return new NewsItemView(getContext());
    }

    @Override
    public BitmapRequest createRequest(News data, int imageIndex, int requestImageWidth, int requestImageHeight) {
        File tempFile = new File(getContext().getCacheDir(), "THUMB_IMAGE_TEMP_" + data.getId());
        return new BitmapRequest(data.getImgUrl(), requestImageWidth, requestImageHeight, tempFile);
    }
}
