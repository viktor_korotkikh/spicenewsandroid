package com.viktorkorotkikh.spicenews.ui;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.spicelist.simple.BitmapSpiceManager;
import com.viktorkorotkikh.spicenews.BuildConfig;
import com.viktorkorotkikh.spicenews.R;
import com.viktorkorotkikh.spicenews.model.News;
import com.viktorkorotkikh.spicenews.network.NewsRequest;
import com.viktorkorotkikh.spicenews.network.NewsService;
import com.viktorkorotkikh.spicenews.storage.Storage;

import java.util.List;

public class NewsListActivity extends Activity {
    private static final String TAG = NewsListActivity.class.getSimpleName();

    private ListView newsListView;
    private View loadingView;

    private NewsListAdapter newsListAdapter;

    private SpiceManager spiceManager = new SpiceManager(NewsService.class);
    private BitmapSpiceManager bitmapSpiceManager = new BitmapSpiceManager();
    private NewsRequest newsRequest = new NewsRequest();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newslist);

        newsListView = (ListView) findViewById(R.id.newslistview);
        loadingView = findViewById(R.id.loading_layout);
    }

    @Override
    public void onStart() {
        super.onStart();
        spiceManager.start(this);
        bitmapSpiceManager.start(this);

        //get data from storage if it possible
        loadNews();
    }

    @Override
    public void onStop() {
        spiceManager.shouldStop();
        bitmapSpiceManager.shouldStop();
        super.onStop();
    }

    private void showProgress() {
        loadingView.setVisibility(View.VISIBLE);
        newsListView.setVisibility(View.GONE);
    }

    private void hideProgress() {
        newsListView.setVisibility(View.VISIBLE);
        loadingView.setVisibility(View.GONE);
    }

    private SpiceManager getSpiceManager() {
        return spiceManager;
    }

    private void updatelistViewContent(List<News> data) {
        newsListAdapter = new NewsListAdapter(this, bitmapSpiceManager, data);
        newsListView.setAdapter(newsListAdapter);
        hideProgress();
    }

    /**
     * Load data from storage if it possible or update from server
     */
    private void loadNews() {
        List<News> news = Storage.get().getNews();
        logNews(news);
        if (news.size() > 0) updatelistViewContent(news);
        else refresh();
    }

    private void logNews(List<News> news) {
        if (BuildConfig.DEBUG) {
            Log.i(TAG, "news:" + news.size());
            for (News n : news) {
                Log.i(TAG, n.toString());
            }
        }
    }

    public void refresh() {
        showProgress();
        getSpiceManager().execute(newsRequest, "news", DurationInMillis.ALWAYS_EXPIRED, new ListNewsRequestListener());
    }

    public final class ListNewsRequestListener implements RequestListener<News.List> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            if (BuildConfig.DEBUG) Log.i(TAG, "onRequestFailure");
//            hideProgress();
            Toast.makeText(getBaseContext(), "Failure update news", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestSuccess(News.List result) {
            if (BuildConfig.DEBUG) Log.i(TAG, "onRequestSuccess");
            hideProgress();
            Toast.makeText(getBaseContext(), "Success update news", Toast.LENGTH_SHORT).show();
            updatelistViewContent(result);
        }
    }
}
