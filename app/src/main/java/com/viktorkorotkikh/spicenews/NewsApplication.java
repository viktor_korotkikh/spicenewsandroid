package com.viktorkorotkikh.spicenews;

import android.app.Application;

public class NewsApplication extends Application {

    private static NewsApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        this.instance = this;
    }

    public static NewsApplication getInstance() {
        return instance;
    }
}
