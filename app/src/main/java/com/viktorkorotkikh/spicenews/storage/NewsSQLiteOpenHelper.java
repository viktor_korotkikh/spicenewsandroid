package com.viktorkorotkikh.spicenews.storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.viktorkorotkikh.spicenews.model.News;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class NewsSQLiteOpenHelper extends SQLiteOpenHelper {

    private final static String DB_FILE = "data.db";
    private final static int DB_VERSION = 1;

    static {
        // register our models
        cupboard().register(News.class);
    }

    public NewsSQLiteOpenHelper(Context context) {
        super(context, DB_FILE, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // this will ensure that all tables are created
        cupboard().withDatabase(db).createTables();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // this line  will upgrade your database, adding columns and new tables.
        // Note that existing columns will not be converted from what they originally were
        cupboard().withDatabase(db).upgradeTables();
    }
}
