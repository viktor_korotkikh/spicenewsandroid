package com.viktorkorotkikh.spicenews.storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.viktorkorotkikh.spicenews.NewsApplication;
import com.viktorkorotkikh.spicenews.model.News;

import java.util.ArrayList;
import java.util.List;

import nl.qbusict.cupboard.DatabaseCompartment;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class Storage {

    NewsSQLiteOpenHelper openHelper;
    SQLiteDatabase database;
    DatabaseCompartment dataCompartment;

    private static Storage instance;
    private static final Object INIT_LOCK = new Object();

    private Storage(Context context) {
        this.openHelper = new NewsSQLiteOpenHelper(context);
        this.database = openHelper.getWritableDatabase();
        this.dataCompartment = cupboard().withDatabase(database);
    }

    public static Storage get() {
        if (instance == null) {
            synchronized (INIT_LOCK) {
                if (instance == null) {
                    instance = new Storage(NewsApplication.getInstance());
                }
            }
        }

        return instance;
    }

    //save jnone news to db
    public Long saveOneNews(News news) {
        return dataCompartment.put(news);
    }

    //save all news to db
    public void saveNews(ArrayList<News> news) {
        dataCompartment.put(news);
    }

    //read all news from db
    public List<News> getNews() {
        return dataCompartment.query(News.class).list();
    }

    //clean all news
    public int deleteAllNews() {
        return database.delete(News.class.getSimpleName(), null, null);
    }
}
