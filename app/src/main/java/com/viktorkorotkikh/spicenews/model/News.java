package com.viktorkorotkikh.spicenews.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class News implements Serializable {

    @SerializedName("nid")
    private long _id; // for cupboard
    private String title;
    private String img_url;

    public static class List extends ArrayList<News> {
    }

    public News() {
    }

    public News(Long id, String title, String img_url) {
        this._id = id;
        this.title = title;
        this.img_url = img_url;
    }

    @Override
    public String toString() {
        return super.toString() + ":" + getId();
    }

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        this._id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImgUrl() {
        return img_url;
    }

    public void setImgUrl(String img_url) {
        this.img_url = img_url;
    }
}
