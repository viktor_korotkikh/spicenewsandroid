package com.viktorkorotkikh.spicenews.network;


import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;

public class NewsService extends RetrofitGsonSpiceService {

    @Override
    public void onCreate() {
        super.onCreate();
        addRetrofitInterface(Api.class);
    }

    @Override
    protected String getServerUrl() {
        return Api.API_URL;
    }
}
