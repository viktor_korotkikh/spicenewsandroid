package com.viktorkorotkikh.spicenews.network;

import android.util.Log;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.viktorkorotkikh.spicenews.BuildConfig;
import com.viktorkorotkikh.spicenews.model.News;
import com.viktorkorotkikh.spicenews.storage.Storage;

public class NewsRequest extends RetrofitSpiceRequest<News.List, Api> {
    public NewsRequest() {
        super(News.List.class, Api.class);
    }

    @Override
    public News.List loadDataFromNetwork() throws Exception {
        News.List news = getService().getNews();

        //rewrite news
        if (news != null) {
            Storage.get().deleteAllNews();
            Storage.get().saveNews(news);
        }

        if (BuildConfig.DEBUG)
            Log.d(NewsRequest.class.getSimpleName(), "loadDataFromNetwork : " + (news == null ? "null" : news.size()));
        return news;
    }
}
