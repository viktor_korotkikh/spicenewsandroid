package com.viktorkorotkikh.spicenews.network;

import com.viktorkorotkikh.spicenews.model.News;

import retrofit.http.GET;

public interface Api {

    public static final String API_URL = "http://api.innogest.ru";

    // Get list of news
    @GET("/api/v3/amobile/news")
    public News.List getNews();
}
